/////////////////////
// The game object //
/////////////////////
var Game = function(canvasId, width, height) {
    this.coq = new Coquette(this, canvasId, width, height, '#fff');
    
    this.STATE = {
        LOADING:  0,
        INTRO:    1,
        MENU:     2,
        COUNTING: 3,
        PLAYING:  4,
        GAMEOVER: 5
    };
    this.state = this.STATE.LOADING;

    this.debug = false;

    this.title = '10 Second Dash';
    this.subtitle = 'Can you beat the 10 second barrier?';

    this.camera = new Camera(this, {pos: {x:0,y:0}, size: {x:width,y:height}});

    // Very ugly
    var self = this;
    var img = new Image();
    img.src = '/lib/img/runners/runningAnimation/shadow.png';
    img.addEventListener('load', function() {
        var image = this;
        self.coq.entities.create(RunnerAnimation, {pos: {x:640-64, y:360-64}},
            function(loadingrunner) {
                self.loadingRunner = loadingrunner;
                loadingrunner.spritesheet = image;
            }
        );
    });
};
Game.prototype.init = function() {
    this.state = this.STATE.INTRO;
    console.log('Game initialized');

    // Destroy loading runner
    this.coq.entities.destroy(this.loadingRunner);

    // Display intro/splash screen for 4000 ms
    var self = this;
    setTimeout(function() {
        self.menu();
    }, 0);//4000);
};
Game.prototype.menu = function() {
    this.state = this.STATE.MENU;
    console.log('Game menu');

    // Remove all objects
    for (var i in this.coq.entities.all()) {
        obj = this.coq.entities.all()[i];
        if (obj instanceof Runner) {
            obj.destroy();
        }
    }

    // Runner
    var self = this,
        w = this.coq.renderer.width,
        h = this.coq.renderer.height;
    this.coq.entities.create(RunnerAnimation, {pos: {x:w/2-32, y:-360+h/2-64}},
        function(menurunner) {
            self.menuRunner = menurunner;
            menurunner.spritesheet = ASSETS.getAsset('/lib/img/runners/runningAnimation/shadow.png');
        }
    );

    // Play sound
    this.menuSound = createjs.Sound.play('menu');
    this.menuSound.setVolume(0.2);

    // Draw menu
    this.menuCanvas = document.createElement('canvas');
    this.menuCanvas.width = w;
    this.menuCanvas.height = h;
    var ctx = this.menuCanvas.getContext('2d');
    // Sunset
    ctx.fillStyle = '#8a1840';
    ctx.fillRect(0, 0, w, h/2);
    ctx.fillStyle = 'rgb(230,145,56)';
    ctx.arc(w/2, h/2, w/5, 0, Math.PI*2, true);
    ctx.fill();
    ctx.fillStyle = '#000';
    ctx.fillRect(0, h/2, w, h/2);
    // Text
    ctx.fillStyle = '#fff';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '16px "Press Start 2P"';
    ctx.fillText('Press ENTER or SPACE to play', w/2, 3*h/5);
    ctx.textAlign = 'start';
    ctx.textBaseline = 'top';
    ctx.fillText(this.title, w/16-16, 16);
    // Controls
    ctx.font = '12px "Press Start 2P"';
    ctx.textAlign = 'end';
    ctx.textBaseline = 'top';
    ctx.fillText('Press M to mute', w-w/16+16, 16);
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillText('Repeatedly hit the keys, ', w/2, h-2*h/16);
    ctx.fillText('moving the legs one at a time', w/2, h-h/16);
    ctx.textBaseline = 'middle';
    ctx.textAlign = 'start';
    ctx.fillText('left leg', w/16+32, 7*h/10+16);
    ctx.textAlign = 'end';
    ctx.fillText('right leg', w-(w/16+32), 7*h/10+16);
    ctx.drawImage(ASSETS.getAsset('/lib/img/leftarrow.png'), w/16-16, 7*h/10);
    ctx.drawImage(ASSETS.getAsset('/lib/img/rightarrow.png'), w-w/16-16, 7*h/10);

};
Game.prototype.start = function() {
    this.state = this.STATE.PLAYING;
    console.log('Game Started');

    this.coq.entities.destroy(this.menuRunner);
    this.menuSound.stop();

    var self = this;
    this.coq.entities.create(Player, {pos: {x:-16, y:36*7}, lane: 8, name: 'Player 1', zindex: 10},
        function(player) {
            self.player = player;
            player.spritesheet = ASSETS.getAsset('/lib/img/runners/default.png');
            player.frames = {
                start: 0,
                running: {
                    start: 1,
                    end: 8,
                    length: 8
                },
                finish: 9
            };
        }
    );
    
    this.runners = [];
    var names = [
        'M. Bolt',   'U. Powell', 'T. Gatlin',   'A. Bailey',  'R. Blake',
        'M. Robson', 'Y. Gay',    'R. Thompson', 'G. Whatson', 'A. Maguire'
    ];
    var spritesheets = ['asian1', 'european1', 'european2', 'africanamerican'];
    for (var i=0; i<7; i++) {
        var name = names[Math.floor(Math.random() * names.length)];
        names.splice(names.indexOf(name), 1);
        this.coq.entities.create(CPU, {pos: {x:-16, y:36*i}, lane: i+1, name: name, zindex: i+3},
            function(runner) {
                self.runners.push(runner);
                var sprite = spritesheets[Math.floor(Math.random() * spritesheets.length)];
                runner.spritesheet = ASSETS.getAsset('/lib/img/runners/'+sprite+'.png');
                runner.frames = {
                    start: 0,
                    running: {
                        start: 1,
                        end: 8,
                        length: 8
                    },
                    finish: 9
                };
            }
        );
    }

    this.stadiumSound = createjs.Sound.play('stadium');
    this.runningSound = createjs.Sound.play('run2');
    this.stadiumSound.setVolume(1);
    this.runningSound.setVolume(0.5);

    this.counting();
};
Game.prototype.counting = function() {
    this.state = this.STATE.COUNTING;
    console.log('Count down: ');

    // Begin race in 3 seconds (3... 2... 1... GO!)
    this.counter = 3;
    var self = this;
    var count = function() {
        setTimeout(function() {
            if (self.counter > 0) {
                console.log('  ' + self.counter);
                self.counter--;
                count();
            } else {
                self.begin();
            }
        }, 1000);
    };

    // Startlist
    this.coq.entities.create(Startlist, {pos: {x:-320, y:0}, zindex:12},
        function(startlist) {
            setTimeout(function() {
                count();
                startlist.destroy();
            }, 5000);
        }
    );
};
Game.prototype.begin = function() {
    this.state = this.STATE.PLAYING;

    this.shotSound = createjs.Sound.play('shot');
    this.shotSound.setVolume(1);

    this.player.start();
    for (var runner in this.runners) {
        this.runners[runner].start();
    }

    this.hasFinished = 0;

    console.log('GO!');
};
Game.prototype.gameover = function() {
    this.state = this.STATE.GAMEOVER;

    this.runningSound.stop();

    // Has dimensions: 480x320 centered in image of 640x360
    this.scoreboard = ASSETS.getAsset('/lib/img/scoreboard.png');
    // Draw scoreboard
    this.scoreCanvas = document.createElement('canvas');
    this.scoreCanvas.width = this.coq.renderer.width;
    this.scoreCanvas.height = this.coq.renderer.height;
    var ctx = this.scoreCanvas.getContext('2d');
    var top = 20+10,
        left = 80+10,
        right = 560-10,
        bottom = 340-10;
    // Text settings
    ctx.font = '16px "Press Start 2P"';
    ctx.fillStyle = '#ffff00';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'top';
    // Text shadow
    ctx.shadowColor = '#ffcc00';//'#ffff00';
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.shadowBlur = 10;
    // Draw text
    ctx.fillText('MEN\'S 100M', right-240, top);
    ctx.fillText('FINAL',       right-240, top+20);
    ctx.fillText('Press R to reset', 320, bottom-20);
    // Scores
    var scores = [];
    scores.push({
        lane: this.player.lane,
        name: this.player.name,
        time: this.player.time
    });
    for (var runner in this.runners) {
        var runner = this.runners[runner];
        scores.push({
            lane: runner.lane,
            name: runner.name,
            time: runner.time
        });
    }
    // Sort by time
    scores.sort(function(a, b) {
        var keyA = a.time,
            keyB = b.time;
        if (keyA < keyB) return -1;
        if (keyA > keyB) return 1;
        return 0;
    });
    // Draw on scoreboard
    if (scores.length > 0) {
        for (var i in scores) {
            var score = scores[i];
            ctx.textAlign = 'start';
            ctx.fillText(score.lane, left,     top+80+24*(i-1));
            ctx.fillText(score.name, left+80,  top+80+24*(i-1));
            ctx.textAlign = 'end';
            var t = (score.time/1000).toFixed(2); // ms => sec & fix to 2 decimals
            ctx.fillText(t, right, top+80+24*(i-1));
        }
    }
};
Game.prototype.update = function(tick) {
    var inputter = this.coq.inputter;
    if (inputter.state(inputter.M)) {
        createjs.Sound.setMute(!createjs.Sound.getMute());
    }
        
    if (this.state == this.STATE.MENU) {
        this.camera.followPos(320, -180);
        this.camera.update(tick);

        if (inputter.state(inputter.ENTER) || inputter.state(inputter.SPACE)) {
            this.start(); // Start game
        }
    }
    if (this.state == this.STATE.PLAYING) {
        this.camera.followPos(this.player.pos.x+32, 180);
        this.camera.update(tick);
        
        // If all runners has crossed the finish line
        if (this.hasFinished == 8) {
            this.gameover();
        }
        
        if (this.coq.inputter.state(this.coq.inputter.D)) {
            this.debug = !this.debug;
        }

        if (inputter.state(inputter.R)) {
            createjs.Sound.stop();
            this.menu();
        }
    }
    if (this.state == this.STATE.COUNTING) {
        this.camera.followPos(0, 180);
        this.camera.update(tick);
    }
    if (this.state == this.STATE.GAMEOVER) {
        this.camera.followPos(2240-360+80+320, 180, 2);
        this.camera.update(tick);

        if (inputter.state(inputter.R)) {
            createjs.Sound.stop();
            this.menu();
        }
    }
};
Game.prototype.draw = function(ctx) {
    if (this.state == this.STATE.PLAYING
    || this.state == this.STATE.COUNTING
    || this.state == this.STATE.GAMEOVER
    || this.state == this.STATE.MENU) {
        this.camera.prep(ctx);
    }

    //Set draw settings
    ctx.font = '40px "Press Start 2P"';
    ctx.fillStyle = '#000';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';

    var renderer = this.coq.renderer,
        w = renderer.width,
        h = renderer.height,
        top = this.camera.borders.top,
        left = this.camera.borders.left;
    // Draw loading screen
    if (this.state == this.STATE.LOADING) {
        ctx.font = '16px "Press Start 2P"';
        ctx.textAlign = 'end';
        ctx.textBaseline = 'bottom';
        ctx.fillText('LOADING', w-72, h-8);
    }
    // Draw intro screen
    else if (this.state == this.STATE.INTRO) {
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(this.title, w/2, h/2);
        ctx.font = '16px "Press Start 2P"';
        ctx.fillText(this.subtitle, w/2, h/2+50, 560);
    }
    // Draw menu screen
    else if (this.state == this.STATE.MENU) {
        ctx.drawImage(this.menuCanvas, 0, -360);
    }
    // Counting down from 3 to GO!
    else if (this.state == this.STATE.COUNTING) {
        var track = ASSETS.getAsset('/lib/img/track.png');
        var tracktile = ASSETS.getAsset('/lib/img/tracktile.png');
        ctx.drawImage(track, -360+80, 0);
        ctx.drawImage(tracktile, -360, 0);
        ctx.drawImage(tracktile, 1960, 0);

        if (this.counter < 3) {
            ctx.fillStyle = '#000';
            ctx.font = '24px "Press Start 2P"';
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            if (this.counter > 0)
                ctx.fillText(this.counter, left+3*w/4, top+h/2);
            else
                ctx.fillText('GO!', left+3*w/4, top+h/2);
        }
    }
    // For the playing screen
    else if (this.state == this.STATE.PLAYING) {
        var track = ASSETS.getAsset('/lib/img/track.png');
        var tracktile = ASSETS.getAsset('/lib/img/tracktile.png');
        ctx.drawImage(track, -360+80, 0);
        ctx.drawImage(tracktile, -360, 0);
        ctx.drawImage(tracktile, 1960, 0);

        // Draw debug
        if (false && this.debug) {
            ctx.fillStyle = 'rgba(255,255,255,0.25)';
            ctx.fillRect(left+8, top+8, 208, 128);
            ctx.fillStyle = '#000';
            ctx.font = '16px "Press Start 2P"';
            ctx.textAlign = 'start';
            ctx.textBaseline = 'alphabetic';
            ctx.fillText('DEBUGGING:', left+16, top+32);

            ctx.moveTo(left+16, top+32);
            ctx.lineTo(left+172, top+32);
            ctx.stroke();

            ctx.font = '12px "Press Start 2P"';
            var yt = top+48,
                dy = 16;
            ctx.fillText('pos: ' + Math.round(this.player.pos.x*10)/10 + ', ' + this.player.pos.y, left+16, yt+dy*0);
            ctx.fillText('speed: ' + this.player.speed + ' m/s', left+16, yt+dy*1);
            ctx.fillText('time: ' + this.player.time/1000 + ' s', left+16, yt+dy*4);
            ctx.fillText('dist: ' + Math.round(this.player.dist > 100 ? 100 : this.player.dist) + ' m', left+16, yt+dy*5);
        }
    }
    // Show score
    else if (this.state == this.STATE.GAMEOVER) {
        var track = ASSETS.getAsset('/lib/img/track.png');
        var tracktile = ASSETS.getAsset('/lib/img/tracktile.png');
        ctx.drawImage(track, -360+80, 0);
        ctx.drawImage(tracktile, -360, 0);
        ctx.drawImage(tracktile, 1960, 0);

        ctx.drawImage(this.scoreboard, 2240-360+80, 0);
        ctx.drawImage(this.scoreCanvas, 2240-360+80, 0);
    }
};



////////////////////////////////////////////////
// Start the game, when the window has loaded //
////////////////////////////////////////////////
var ASSETS = new AssetManager();
window.addEventListener('load', function() {
    //Set canvas and start game
    var game = new Game('canvas', 640, 360);

    //Add image resources to the download queue
    ASSETS.queueDownload('/lib/img/track.png');
    ASSETS.queueDownload('/lib/img/tracktile.png');
    ASSETS.queueDownload('/lib/img/leftarrow.png');
    ASSETS.queueDownload('/lib/img/rightarrow.png');
    ASSETS.queueDownload('/lib/img/scoreboard.png');
    ASSETS.queueDownload('/lib/img/runners/default.png');
    ASSETS.queueDownload('/lib/img/runners/asian1.png');
    ASSETS.queueDownload('/lib/img/runners/european1.png');
    ASSETS.queueDownload('/lib/img/runners/european2.png');
    ASSETS.queueDownload('/lib/img/runners/africanamerican.png');
    ASSETS.queueDownload('/lib/img/runners/runningAnimation/shadow.png');

    // When finished downloading image resources,
    // download sounds
    ASSETS.downloadAll(function() {
        //Add sounds to be preloaded
        var soundQueue = new createjs.LoadQueue();
        soundQueue.installPlugin(createjs.Sound);
        soundQueue.addEventListener('complete', handleComplete);
        soundQueue.loadFile( {id:'menu', src:'/lib/snd/title-music.mp3'} );
        // soundQueue.loadFile( {id:'run1', src:'/lib/snd/run1.mp3'} );
        soundQueue.loadFile( {id:'run2', src:'/lib/snd/run2.mp3'} );
        // soundQueue.loadFile( {id:'run3', src:'/lib/snd/run3.mp3'} );
        soundQueue.loadFile( {id:'stadium', src:'/lib/snd/stadium1.mp3'} );
        soundQueue.loadFile( {id:'shot', src:'/lib/snd/shot1.mp3'} );
        soundQueue.loadFile( {id:'leftfoot', src:'/lib/snd/left.mp3'} );
        soundQueue.loadFile( {id:'rightfoot', src:'/lib/snd/right.mp3'} );
    });

    // When sounds are completed,
    // begin game!
    var handleComplete = function() {
        game.init();
    };
});