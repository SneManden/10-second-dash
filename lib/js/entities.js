//////////////
// Entities //
//////////////

/**
 * A player object
 * 16 px = 1 m
 */
var Runner = function(game, settings) {
    this.game = game;
    for (var i in settings) {
        this[i] = settings[i];
    }
    this.size = {x:64, y:64};
    this.canMove = false;
    this.speed = 0; // meter/sec (max speed should be around 12.5)
    this.fstep = 0;
    this.frame = 0;
    this.fspeed = 0; // frame speed (1: every step, 60: every sec)
    this.STATE = {
        START: 0,
        RUNNING: 1,
        FINISH: 2
    };
    this.state = this.STATE.START;

    this.dist = 0;
    this.time = 0;

    this.ready = true;
    this.delay = 50; // Delay between keystrokes
    this.LEG = { LEFT: 0, RIGHT: 1 };
    this.leg = this.LEG.LEFT;
    this.prev = 0;
};
Runner.prototype.start = function() {
    this.state = this.STATE.RUNNING;
    this.canMove = true;
    this.time = 0;
};
Runner.prototype.update = function(tick) {
    var inputter = this.game.coq.inputter;

    // Allow keystrokes to be triggered every 50 ms
    if (!this.ready && (new Date().getTime()) - this.prev > this.delay) {
        this.ready = true;
    }

    // You lose speed all the time
    if (this.speed > 0.1) {
        this.speed = Math.round(this.speed*10-1)/10;
    } else { // Set frame to standing position when speed < 0.1
        this.speed = 0;
        this.frame = 1;
    }


    // Do animation
    this.fspeed = Math.round(Math.pow(Math.E, -0.25*this.speed+3));
    if (this.fspeed < 20) {
        if (this.fstep >= this.fspeed) {
            this.frame = (this.frame%this.frames.running.length)+1; // THIS IS NOT CORRECT
            this.fstep = 0;
        } else {
            this.fstep++;
        }
    } else {
        this.fstep = 0;
        this.frame = 2;
    }


    // Update position and distance
    this.pos.x += 16*this.speed*(tick/1000);
    this.dist += this.speed*(tick/1000);

    // Update time
    if (this.state == this.STATE.RUNNING) {
        this.time = Math.round(this.time+tick);

        // If we have reached 100 m => finish
        if (this.dist >= 100) {
            this.finish();
        }
    }
};
Runner.prototype.run = function() {
    if (this.leg == this.LEG.LEFT) {
        this.leg = this.LEG.RIGHT;
    } else {
        this.leg = this.LEG.LEFT;
    }
    this.speed += (5/(this.speed+1));
    this.ready = false;
    this.prev = new Date().getTime();
};
Runner.prototype.draw = function(ctx) {    
    var frame;
    if (this.state == this.STATE.START)
        frame = this.frames.start;
    else {
        if (this.speed > 0)
            frame = this.frame;
        else
            frame = this.frames.finish;
    }

    var image = this.spritesheet,
        sx = this.size.x*frame,
        sy = 0,
        sw = this.size.x,
        sh = this.size.y,
        dx = this.pos.x,
        dy = this.pos.y,
        dw = this.size.x,
        dh = this.size.y;
    ctx.drawImage(image, sx, sy, sw, sh, dx, dy, dw, dh);
};
Runner.prototype.finish = function() {
    this.state = this.STATE.FINISH;
    this.canMove = false;

    this.game.hasFinished++;

    console.log(this.name + ' finished 100m in time: ' + (this.time/1000).toFixed(2));
};
Runner.prototype.destroy = function() {
    this.game.coq.entities.destroy(this);
};



/**
 * Player is a runner
 */
var Player = function(game, settings) {
    Runner.call(this, game, settings);
    this.penalty = 0.5; // 0.1;
};
Player.prototype = new Runner();
Player.prototype.constructor = Player;
Player.prototype.update = function(tick) {
    var inputter = this.game.coq.inputter;

    if (this.canMove) {
        if (this.ready) {
            // LEFT LEG
            if (this.leg == this.LEG.LEFT) {
                // Penalty
                if (inputter.state(inputter.RIGHT_ARROW))
                    this.speed -= this.penalty;

                if (inputter.state(inputter.LEFT_ARROW))
                    this.run();
            }
            // RIGHT LEG
            else if (this.leg == this.LEG.RIGHT) {
                // Penalty
                if (inputter.state(inputter.LEFT_ARROW))
                    this.speed -= this.penalty;

                if (inputter.state(inputter.RIGHT_ARROW))
                    this.run();
            }
        }
    }

    Runner.prototype.update.call(this, tick);
};



/**
 * CPU is a runner
 */
var CPU = function(game, settings) {
    Runner.call(this, game, settings);
    this.skill = Math.random()*40 + 10;
};
CPU.prototype = new Runner();
CPU.prototype.constructor = CPU;
CPU.prototype.update = function(tick) {
    if (this.canMove) {
        if (this.ready) {
            this.run();
            this.delay = Math.random()*this.skill + 50;
        }
    }

    Runner.prototype.update.call(this, tick);
};



/**
 * Start list for displaying competitiants (spelling...)
 */
var Startlist = function(game, settings) {
    this.game = game;
    for (var i in settings) {
        this[i] = settings[i];
    }
    this.size = { x: 0, y: 0};
    // this.drawStartlist();
};
Startlist.prototype.drawStartlist = function() {
    this.image = document.createElement('canvas');
    this.image.width = this.game.coq.renderer.width;
    this.image.height = this.game.coq.renderer.height;
    var ctx = this.image.getContext('2d');

    var top = 80-24,
        left = 160,
        width = 640-320,
        height = 360-160+48+24,
        right = left+width,
        bottom = top+height;

    ctx.fillStyle = 'rgba(0,0,0,0.5)';
    ctx.fillRect(left, top, width, height);

    ctx.font = '16px "Press Start 2P"';
    ctx.fillStyle = '#fff';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'alphabetic';
    ctx.fillText('MEN\'S 100M', left+width/2, top+24);
    ctx.fillText('START LIST: FINAL', left+width/2, top+48);
    ctx.textAlign = 'start';
    for (var i in this.game.runners) {
        var runner = this.game.runners[i];
        ctx.fillText(runner.lane, left+20, bottom-24*(i)-24-12);
        ctx.fillText(runner.name, left+80, bottom-24*(i)-24-12);
    }
    ctx.fillStyle = '#f00';
    ctx.fillText(this.game.player.lane, left+20, bottom-12);
    ctx.fillText(this.game.player.name, left+80, bottom-12);
};
Startlist.prototype.update = function(tick) {
    if (this.game.player && this.game.runners.length == 7) {
        this.drawStartlist();
    }
};
Startlist.prototype.draw = function(ctx) {
    ctx.drawImage(this.image, this.pos.x, this.pos.y);
};
Startlist.prototype.destroy = function() {
    this.game.coq.entities.destroy(this);
};



/**
 * Runner animation for the menu
 */
var RunnerAnimation = function(game, settings) {
    this.game = game;
    for (var i in settings) {
        this[i] = settings[i];
    }
    this.size = {x:64, y:64};
    this.fstep = 0;
    this.frame = 1;
    this.fspeed = 2; // frame speed (1: every step, 60: every sec)
    this.frames = { length: 8 };
};
RunnerAnimation.prototype.update = function(tick) {
    if (this.fstep >= this.fspeed) {
        this.frame = (this.frame+1)%this.frames.length;
        this.fstep = 0;
    } else {
        this.fstep++;
    }
};
RunnerAnimation.prototype.draw = function(ctx) {
    // var frame = this.frames[this.frame];
    // ctx.drawImage(frame, this.pos.x, this.pos.y, this.size.x, this.size.y);
    var image = this.spritesheet,
        sx = this.size.x*this.frame,
        sy = 0,
        sw = this.size.x,
        sh = this.size.y,
        dx = this.pos.x,
        dy = this.pos.y,
        dw = this.size.x,
        dh = this.size.y;
    ctx.drawImage(image, sx, sy, sw, sh, dx, dy, dw, dh);
};