/**
 * Camera extention (for Coquette.js) created by Casper Kehlet Jensen
 *
 * Usage:
 *  - include in html alongside coquette.js
 *      e.g.: <script src="/path/to/camera.js"></script>
 *  - create instance of camera (in game object)
 *      e.g.: this.camera = new Camera(this, {pos:{x:?,y:?}, size:{x:?,y:?}});
 *  - Call update and follow methods in update-method of game object
 *      e.g.: this.camera.follow( <desired object> );
 *            this.camera.update(tick);
 *  - Call prep method in draw method of game object
 *      e.g.: this.camera.prep(ctx);
 * 
 * Supports a neat shake effect:
 *      e.g.: this.camera.shake(60, 10) // shakes camera for 60 frames (1 sec) with force 10
 */
;(function(exports) {
    var Camera = function(game, settings) {
        this.game = game;
        this.pos = settings.pos;
        this.size = settings.size;
        this.updateBorders();
        // Shaking
        this.shaking = false;
        this.force = 10;
        this.step = 0;
        this.max = 0;
    };

    Camera.prototype = {
        update: function(tick) {
            if (this.shaking && this.step > 0) {
                var effect = this.force * (this.step/this.max);
                this.pos.x += Math.random()*effect - (effect/2);
                this.pos.y += Math.random()*effect - (effect/2);
                this.step--;
            } else {
                this.max = 0;
                this.shaking = false;
            }

            this.updateBorders();
        },

        prep: function(ctx) {
            // Viewport, follow obj
            ctx.setTransform(1,0,0,1,0,0);
            ctx.translate(-this.pos.x + this.size.x/2, -this.pos.y + this.size.y/2);
            // Clear rect
            ctx.clearRect(this.pos.x - this.size.x/2, this.pos.y - this.size.y/2, this.size.x, this.size.y);
        },

        follow: function(obj, speed) {
            // Follow obj
            if (speed) { // Move camera to obj.pos with speed
                if (Math.abs(this.pos.x-obj.pos.x) >= speed) {
                    if (this.pos.x > obj.pos.x)
                        this.pos.x -= speed;
                    else
                        this.pos.x += speed;
                } else {
                    this.pos.x = obj.pos.x;
                }
                if (Math.abs(this.pos.y-obj.pos.y) >= speed) {
                    if (this.pos.y > obj.pos.y)
                        this.pos.y -= speed;
                    else
                        this.pos.y += speed;
                } else {
                    this.pos.y = obj.pos.y;
                }
            } else { // Move camera to obj.pos instantly
                this.pos.x = obj.pos.x;
                this.pos.y = obj.pos.y;
            }
        },

        followPos: function(x, y, speed) {
            // Follow position
            if (speed) { // Move camera to obj.pos with speed
                if (Math.abs(this.pos.x-x) >= speed) {
                    if (this.pos.x > x)
                        this.pos.x -= speed;
                    else
                        this.pos.x += speed;
                } else {
                    this.pos.x = x;
                }
                if (Math.abs(this.pos.y-y) >= speed) {
                    if (this.pos.y > y)
                        this.pos.y -= speed;
                    else
                        this.pos.y += speed;
                } else {
                    this.pos.y = y;
                }
            } else { // Move camera to obj.pos instantly
                this.pos.x = x;
                this.pos.y = y;
            }
        },

        updateBorders: function() {
            this.borders = {
                top: this.pos.y - this.size.y/2,
                left: this.pos.x - this.size.x/2,
                right: this.pos.x + this.size.x/2,
                bottom: this.pos.y + this.size.y/2
            };
        },

        shake: function(steps, force, debug) {
            if (force) { this.force = force; }
            else       { this.force = 10; }
            this.max = steps;
            this.step = steps;
            this.shaking = true;

            if (debug) {
                console.log(
                    'Shaking camera with a force of '
                    + force + ' for ' + steps/60
                    + ' seconds.'
                );
            }
        }
    };

    exports.Camera = Camera;
})(this);