#10 Second Dash

Cheering in the background... That magical beat, playing on repeat in your head. This is it. This is the finals. You are amongst the fastest of the fastests. The top 0.00001%. Can you live up to that?
In 10 Second Dash you are THE runner. And you have just one final run left.. Are you the fastest man alive? Will you break The 10 Second Barrier?

10 Second Dash is a naturally fast-paced game that is a perfect time-killer, and a perfect game to compete with your friends in. Take a screenshot of your less-than-10-second-run, and share it with your friends for the ultimate bragging rights!

Credits:
========
 - Project management, beta-tester and motivator: Joakim
 - Main programmer and graphics: Casper
Music:
 - Running music: Blue Metropolis af Mike Rix
 - Menu music: Story of a Wounded Heart af J L T


	//Ludum Dare 27 Jam entry
	//by Joakim & Casper
